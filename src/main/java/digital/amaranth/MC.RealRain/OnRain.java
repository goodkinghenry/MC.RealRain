package digital.amaranth.realrain;

import digital.amaranth.quickblocklib.BiomeGroups;
import digital.amaranth.quickblocklib.UsefulConstants;

import static digital.amaranth.realrain.RealRain.getInstance;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class OnRain implements Listener {
    private final int RAIN_CHECK_INITIAL_TICKS;
    private final int RAIN_CHECK_INTERVAL_TICKS;
    private final int RAIN_PER_PLAYER;
    private final int THUNDER_RAIN_PER_PLAYER;
    private final int RAIN_MAX_RANGE;
    
    public OnRain () {
        this.RAIN_CHECK_INITIAL_TICKS = 100;
        this.RAIN_CHECK_INTERVAL_TICKS = 300;
        this.RAIN_PER_PLAYER = 15;
        this.THUNDER_RAIN_PER_PLAYER = 30;
        this.RAIN_MAX_RANGE = 150;
    }
    
    Random rainRandom = new Random();
    
    public boolean temperatureIsFreezing (Block block) {
        return block.getTemperature() < UsefulConstants.MAX_SNOW_TEMPERATURE;
    }
    
    private void doRainNear (World world, Location location, boolean isThundering) {
        int rainBlocksPerPlayer = isThundering ? THUNDER_RAIN_PER_PLAYER : RAIN_PER_PLAYER;
        for (int i = 0; i < rainBlocksPerPlayer; i++) {
            Location nextLocation = location.clone().add(
                    (rainRandom.nextBoolean() ? 1 : -1) * rainRandom.nextInt(RAIN_MAX_RANGE), 
                    0, 
                    (rainRandom.nextBoolean() ? 1 : -1) * rainRandom.nextInt(RAIN_MAX_RANGE));
            
            Block rainBlock = world.getHighestBlockAt(nextLocation).getRelative(BlockFace.UP);
            
            if (!BiomeGroups.isInAWaterBiome(rainBlock) && !BiomeGroups.isInARainlessBiome(rainBlock)) {
                if (!temperatureIsFreezing(rainBlock)) {
                    rainBlock.breakNaturally();
                    rainBlock.setType(Material.WATER);
                } else {
                    rainBlock.setType(Material.SNOW_BLOCK);
                }
            }
        }
    }
    
    private void doRainOnAll (World world) {
        world.getPlayers().forEach(each -> {
            doRainNear(world, each.getLocation(), world.isThundering());
        });
    }
    
    private void scheduleRainCheck (World world, int ticks) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(getInstance(), () -> {
            if (!world.isClearWeather()) {
                doRainOnAll(world);
                scheduleRainCheck(world, RAIN_CHECK_INTERVAL_TICKS);
            }
        }, ticks);
    }
    
    @EventHandler
    public void onWeatherChangeEvent (WeatherChangeEvent event) {
        if (event.toWeatherState()) {
            scheduleRainCheck(event.getWorld(), RAIN_CHECK_INITIAL_TICKS);
        }
    }
}
