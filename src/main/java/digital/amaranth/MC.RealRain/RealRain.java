package digital.amaranth.realrain;

import digital.amaranth.quickblocklib.QuickBlockLib;

import org.bukkit.Bukkit;

import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;

import org.bukkit.plugin.java.JavaPlugin;

public class RealRain extends JavaPlugin {
    public static RealRain getInstance() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("RealRain");
        if (plugin == null || !(plugin instanceof RealRain)) {
            throw new RuntimeException("'RealRain' not found.");
        }
        
        return ((RealRain) plugin);
    }
    
    @Override
    public void onEnable() {
        QuickBlockLib.setPlugin(this);
        
        getServer().getPluginManager().registerEvents(new OnRain(), this);
    }
    
    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
